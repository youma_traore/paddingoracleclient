package org.dador.paddingOracleClient;


import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Main Class for Padding OracleClient
 */
public class OraclePaddingClient {
    static final String ENCRYPTED_MESSAGE = "5ca00ff4c878d61e1edbf1700618fb287c21578c0580965dad57f70636ea402fa0017c4acc82717730565174e2e3f713d3921bab07cba15f3197b87976525ce4";
    static final int BLOCK_SIZE = 16;

    /**
     * Fonction takes a number and creates a block of x00 values, padded according to PKCS#7
     * example : n=3 result is 00 00 .. 00 03 03 03
     * @param n : number of bytes of padding
     * @return byte[BLOCK_SIZE] filled with 0 and padding values
     */
    protected static byte[] getPaddingArray(int n) {
        byte[] result = new byte[BLOCK_SIZE];

        /**
         * TODO : Your CODE HERE
         */
        return result;
    }

    /**
     * Function that create a modified ciphertext bloc for trying a guess
     * Note that the "ciphertext" correspond to the IV part for the Block Cipher
     * @param ciphertext : original ciphertext bloc
     * @param decoded    : decrypted part of the plain text (for next bloc)
     * @param position   : position of the byte to guess
     * @param guess      : the guess for this query
     * @return a byte array with c0...c(i-1)||ci+i+g||cj+mj+i||...||cn+mn+i
     */
    protected static byte[] buildGuessForPosition(byte[] ciphertext, byte[] decoded, int position, byte guess) {
        byte[] result = new byte[BLOCK_SIZE];
        
        for(int i=0;i<BLOCK_SIZE;i++)
        	result[i]=ciphertext[i];
        
        result[position]=guess;
     

        /**
         * TODO : YOUR CODE HERE
         */
     
        return result;
    }

    /**
     * Fonction that splits a message into constituent blocs of BLOCK_SIZE
     *
     * @param message
     * @return an array of blocs
     * @throws IllegalArgumentException
     */
    protected static byte[][] splitMessageIntoBlocks(byte[] message) throws IllegalArgumentException {
        if (message.length % BLOCK_SIZE != 0) {
            throw new IllegalArgumentException("Message length is not a multiple of bloc size");
        }
        
        int nb_bloc=message.length/16;
        byte [][] blocks = new byte[nb_bloc][16];
        
        
        int i=0;
        int j=0;
        int k=0;
        for(i=0;i<nb_bloc;i++)
        {
        	for(j=0;j<16;j++)
        	{
        		blocks[i][j]=message[k];
        		k++;
        	}
        }
     
        
        /**
         * TODO : YOUR CODE HERE
         */
        return blocks;
    }

    /**
     * Function that takes the 2 last blocks of the message
     * and returns the length of the padding.
     * @param poq : a PaddingOracleQuery object
     * @param previousbloc : next to last block of the ciphertext
     * @param lastbloc : last bloc of the ciphertext
     * @return an integer corresponding to padding length
     * @throws IOException
     * @throws URISyntaxException
     */
    public int getPaddingLengthForLastBlock(PaddingOracleQuery poq, byte[] previousbloc, byte[] lastbloc) throws IOException, URISyntaxException {
        /**
         * TODO : Your Code HERE
         * 
         */
        // should not arrive here !
        return 0;
    }

    /**
     * Main function that takes 2 consecutive blocks of the ciphertext
     * and returns the decryption of the 2nd message block
     *
     * @param poq : a PaddingOracleQuery object to query server
     * @param iv : the "iv" part of the 2 blocks query
     * @param ciphertext : the block that will be decrypted
     * @param padding : set to 0 if not the last block. Set to paddinglength if last block
     * @return a decrypted byte array
     * @throws IOException
     * @throws URISyntaxException
     */
    public static byte[] runDecryptionForBlock(PaddingOracleQuery poq, byte[] iv, byte[] ciphertext, int padding) throws IOException, URISyntaxException {
        byte[] decoded = new byte[BLOCK_SIZE];
        if (padding > 0) {
            decoded = getPaddingArray(padding);
        }
        HexConverters convertion= new HexConverters();
        for(int i=0;i<256;i++) {
        	
        	
            byte [] block= buildGuessForPosition(iv, ciphertext, BLOCK_SIZE-1, (byte) i);
     
            String block1=convertion.toHexFromByteArray(block);
            String block2=convertion.toHexFromByteArray(ciphertext);
            String mess=block1+block2;
            try {
                //System.out.println("Server responded : " + opq.query(mess));
                
                if(poq.query(mess))
                {
                	System.out.println("le num :"+i+" true");
                
                	int m3x=1^i^iv[BLOCK_SIZE-1];
                	System.out.println(m3x);
                	decoded[BLOCK_SIZE-1]=(byte) m3x;
                
                }
            } catch (Exception e) {
                System.out.print("Exception caught. Server down ?");
                e.printStackTrace();
            }
            try {
                /**
                 * TODO : YOUR CODE HERE
                 */
          } catch (Exception e) {
                System.out.print("Exception caught. Server down ?");
                e.printStackTrace();
            }
        }
        
       
        
        
        /**
         * TODO : YOUR CODE HERE
         */
        
        return decoded;
    }

    public static void main(String[] args) throws IOException, URISyntaxException {
    	
        OraclePaddingClient opc = new OraclePaddingClient();
        PaddingOracleQuery opq = new PaddingOracleQuery();
        HexConverters convertion= new HexConverters();
        byte [] message_byte=convertion.toByteArrayFromHex(ENCRYPTED_MESSAGE);
        byte [][] message_split=splitMessageIntoBlocks(message_byte);
     
        byte [] decoded= runDecryptionForBlock(opq,message_split[0],message_split[1],0);
        
        System.out.println(convertion.toPrintableString(decoded));
       
       
       
      
    }

}

